
import java.util.ArrayList;
import java.util.List;


public class TestPessoa {
    
    
    public static void main(String[] args){
    
        Pessoa p = new Pessoa("Jose", "(27) 99999-9999");
        Pessoa p1 = new Pessoa("Maria", "(27) 99999-9999");
        Pessoa p2 = new Pessoa("João", "(27) 99999-9999");
        Pessoa p3 = new Pessoa("Carlos", "(27) 99999-9999");
        Pessoa p4 = new Pessoa("Marta", "(27) 99999-9999");
        Pessoa p5 = new Pessoa("Carol", "(27) 99999-9999");
        Pessoa p6 = new Pessoa("Marcelo", "(27) 99999-9999");
        Pessoa p7 = new Pessoa("Yuri", "(27) 99999-9999");
        Pessoa p8 = new Pessoa("Brian", "(27) 99999-9999");
        Pessoa p9 = new Pessoa("Fatima", "(27) 99999-9999");
        Pessoa p10 = new Pessoa("Ivan", "(27) 99999-9999");
        Pessoa p11 = new Pessoa("Gustavo", "(27) 99999-9999");

        List<Pessoa> listaDePessoas = new ArrayList<>();
        
        listaDePessoas.add(p1);
        listaDePessoas.add(p2);
        listaDePessoas.add(p3);
        listaDePessoas.add(p4);
        listaDePessoas.add(p5);
        listaDePessoas.add(p6);
        listaDePessoas.add(p7);
        listaDePessoas.add(p8);
        listaDePessoas.add(p9);
        listaDePessoas.add(p10);
        listaDePessoas.add(p11);
        
        System.out.println(listaDePessoas.size());
        System.out.println(p2);
        
        for(Pessoa x : listaDePessoas){
        System.out.println(x);
        }
        
        Pessoa pX = new Pessoa("Jose da Couves", "(27) 9999-9999");
        Pessoa pY = new Pessoa("Silas", "(27) 8888-8888");
        
        System.out.println("Jose Está na Lista?");
        System.out.println(listaDePessoas.contains(pX));
        
        System.out.println("Silas Está na Lista?");
        System.out.println(listaDePessoas.contains(pY));
        
        listaDePessoas.remove(pX);
        
        System.out.println("Jose Está na Lista?");
        System.out.println(listaDePessoas.contains(pX));
        
        for(int i = 0 ; i < listaDePessoas.size(); i++){
        System.out.println(listaDePessoas.get(i));
        }
    }
    
}
