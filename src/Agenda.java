
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Agenda {
    
    private List<Pessoa> contatos;
    
    public Agenda() {
        this.contatos = new ArrayList<>();
    }
    
    public void adicionarContato(Pessoa p) {
        
        if (!this.isContatoNaAgenda(p)) {
            this.contatos.add(p);
        } else {
            throw new RuntimeException("Contato ja existente !");
        }
    }
    
    public void removerContato(String nometelefone) {
        
        boolean nex = true;
        for (int i = 0; i < this.contatos.size(); i++) {
            if (this.contatos.get(i).getNome().equalsIgnoreCase(nometelefone)) {
                this.contatos.remove(i);
                nex = false;
            }
        }
        for (int i = 0; i < this.contatos.size(); i++) {
            if (this.contatos.get(i).getTelefone().equalsIgnoreCase(nometelefone)) {
                this.contatos.remove(i);
                nex = false;
            }
        }
        if (nex) {
            throw new RuntimeException("Contato inexiste!");
        }
    }
    
    public int getQuantidadeContatos() {
        return this.contatos.size();
    }
    
    public Pessoa buscarContatoNome(String nome) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome)) {
                return this.contatos.get(i);
            }
        }
        throw new RuntimeException("Contato inexiste!");
    }
    
    public Pessoa buscarContatoTel(String telefone) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getTelefone().equals(telefone)) {
                return this.contatos.get(i);
            }
        }
        throw new RuntimeException("Contato inexiste!");
    }
    
    public boolean isContatoNaAgenda(Pessoa p) {
        return this.contatos.contains(p);
    }
    
    public String getAgendaTelefone(int i) {
        
        return this.contatos.get(i).getTelefone();
        
    }
    
    public String getAgendaNome(int i) {
        
        return this.contatos.get(i).getNome();
        
    }
    
    public String testListaVazia() {
        
        if (this.contatos.isEmpty()) {
            
            throw new RuntimeException("A lista de contatos estÃ¡ vazia!");
        }
        
        return null;
    }
    
    public String ListaFull() {
        if (this.getQuantidadeContatos() == 0) {
            throw new RuntimeException("Lista vazia!");
        } else {
            String list = "";
            for (int i = 0; i < this.getQuantidadeContatos(); i++) {
                list += i + 1 + "\n" + "Nome: " + this.contatos.get(i).getNome() + "\n"
                        + "Telefone: " + this.contatos.get(i).getTelefone() + "\n\n";
            }
            return list;
        }
    }
    
    public String VerificarContato(String nome, String telefone) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome) && this.contatos.get(i).getTelefone().equals(telefone)) {
                throw new RuntimeException("Contato jÃ¡ existente");
            }
        }
        return null;
    }
    
    public String VerificarNome(String nome) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome)) {
                return "O nome jÃ¡ Ã© cadastrado deseja continuar assim mesmo?";
            }
        }
        return null;
    }
    
    public String VerificarTelefone(String telefone) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getTelefone().equals(telefone)) {
                return "O telefone jÃ¡ Ã© cadastrado deseja continuar assim mesmo?";
            }
        }
        return null;
    }
    
    public boolean ExisteNome(String nome) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean ExisteTelefone(String telefone) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getTelefone().equals(telefone)) {
                return true;
            }
        }
        return false;
    }
    
    public void gravar() throws IOException {
        
        File file = new File("contatos.txt");
        FileWriter fw;
        
        if (!file.exists()) {
            
            file.createNewFile();
            fw = new FileWriter(file);
        } else {
            fw = new FileWriter(file);
        }
        
        for (Pessoa p : this.contatos) {
            fw.write(p.getNome() + "@#@" + p.getTelefone() + "\n");
        }
        
        fw.close();
        
    }
    
}